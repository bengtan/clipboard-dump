const input = document.getElementById('input')
const textOutput = document.getElementById('text-output')
const htmlOutputPre = document.getElementById('html-output-pre')
const htmlOutputDom = document.getElementById('html-output-dom')

function handlePaste(e) {
    e.preventDefault()
    const text = e.clipboardData.getData('text')
    const html = e.clipboardData.getData('text/html')

    console.log('html:')
    console.log(html)

    textOutput.innerText = text
    htmlOutputPre.innerText = html
    htmlOutputDom.innerHTML = html
}

function main() {
    input.addEventListener('paste', handlePaste)
}

main()
